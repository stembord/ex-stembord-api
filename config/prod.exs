use Mix.Config

config :stembord_api, Stembord.Api.Web.Endpoint,
  http: [:inet6, port: 4000],
  url: [host: "localhost", port: 4000]

config :logger, level: :info

config :peerage, via: Peerage.Via.Dns,
  dns_name: "api-stembord-api.api"

config :stembord_api, Stembord.Api.Repo,
  url: "stembord_prod",
  pool_size: 15,
  show_sensitive_data_on_connection_error: false
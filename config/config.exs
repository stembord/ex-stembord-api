use Mix.Config

config :stembord_api,
  ecto_repos: [Stembord.Api.Repo],
  generators: [binary_id: true]

config :stembord_api, Stembord.Api.Web.Endpoint,
  url: [host: "localhost"],
  check_origin: false,
  secret_key_base: "wF2JEGZt8htPRCCWxuDaB05lZDEhg8pIH72uX3LUsEW9JhnyBtOljer7JN4wyEdY",
  render_errors: [view: Stembord.Api.Web.View.Error, accepts: ~w(json)],
  pubsub: [name: Stembord.Api.PubSub, adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

config :stembord_api, Stembord.Api.Scheduler, debug_logging: true

config :peerage, via: Peerage.Via.Dns,
      dns_name: "localhost",
      app_name: "stembord_api",
      log_results: false

config :cors_plug,
  origin: ~r/.*/,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"]

config :stembord_api, Stembord.Api.Repo,
  url: System.get_env("DATABASE_URL")

config :joken,
  default_signer: System.get_env("JWT_SECRET_KEY")

config :arc,
  storage: Arc.Storage.Local,
  storage_dir: "priv/static/assets"

import_config "#{Mix.env()}.exs"

use Mix.Config

config :stembord_api, Stembord.Api.Web.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn

config :stembord_api, Stembord.Api.Repo,
  url: System.get_env("TEST_DATABASE_URL"),
  pool: Ecto.Adapters.SQL.Sandbox,
  show_sensitive_data_on_connection_error: true
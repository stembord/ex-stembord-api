use Mix.Config

config :stembord_api, Stembord.Api.Web.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  watchers: []

config :logger, :console, 
  format: "[$level] $message\n",
  level: :info

config :phoenix, :stacktrace_depth, 20

config :phoenix, :plug_init_mode, :runtime

config :stembord_api, Stembord.Api.Repo,
  database: "stembord_dev",
  pool_size: 10,
  show_sensitive_data_on_connection_error: true
FROM elixir:1.8

ARG MIX_ENV=prod

RUN mix local.hex --force
RUN mix local.rebar --force

WORKDIR /app
COPY . /app

ENV MIX_ENV=${MIX_ENV}
RUN mix deps.get
RUN mix deps.compile
# RUN mix distillery.release --verbose

ENTRYPOINT /app/entrypoint.sh
defmodule Stembord.Api.MixProject do
  use Mix.Project

  def organization(), do: :stembord
  def name(), do: :api
  def version(), do: "0.1.0"

  def project do
    [
      app: String.to_atom("#{organization()}_#{name()}"),
      version: version(),
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Stembord.Api.Application, []},
      extra_applications: [
        :logger, :runtime_tools, :comeonin
      ]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp env(:prod),
    do: [
      DOCKER_REGISTRY: "registry.#{organization()}.com",
      HELM_REPO: "https://chartmuseum.#{organization()}.com"
    ]

  defp env(_),
    do: [
      DOCKER_REGISTRY: "registry.local-k8s.com",
      HELM_REPO: "http://chartmuseum.local-k8s.com"
    ]

  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:uuid, "~> 1.1"},
      {:cors_plug, "~> 2.0"},
      {:plug_cowboy, "~> 2.0"},
      {:peerage, "~> 1.0"},

      # Api
      {:joken, "~> 2.2.0"},
      {:open_api_spex, "~> 3.6"},
      {:aicacia_handler, "~> 0.1"},
      {:arc, "~> 0.11.0"},
      {:arc_ecto, "~> 0.11.3"},

      # Authentication
      {:comeonin, "~> 5.3"},
      {:argon2_elixir, "~> 2.0"},
      {:ueberauth, "~> 0.6"},
      {:ueberauth_google, "~> 0.9"},

      {:distillery, "~> 2.0"}
    ]
  end

  defp namespace(), do: "api"
  defp helm_dir(), do: "./helm/#{organization()}-#{name()}"
  defp get_env(key), do: Keyword.get(env(Mix.env()), key, "")

  defp docker_repository(), do: "#{get_env(:DOCKER_REGISTRY)}/api/#{name()}"
  defp docker_tag(), do: "#{docker_repository()}:#{version()}"

  defp helm_overrides(),
    do:
      "--set image.tag=#{version()} --set image.repository=#{docker_repository()} --set image.hash=$(mix docker.sha256)"

  defp createHelmInstall(values \\ nil),
    do:
      "helm install #{helm_dir()} --name #{name()} --namespace=#{namespace()} #{helm_overrides()} #{
        if values == nil, do: "", else: "--values #{values}"
      }"

  defp createHelmUpgrade(values \\ nil),
    do:
      "helm upgrade #{name()} #{helm_dir()} --namespace=#{namespace()} --install #{
        helm_overrides()
      } #{if values == nil, do: "", else: "--values #{values}"}"

  defp aliases do
    [
      deploy: ["phx.digest", "ecto.setup", "release.init", "release"],
      
      # Dev Postgres
      postgres: [
        "cmd mkdir -p ${PWD}/.volumes/#{name()}-postgres",
        "cmd docker run --rm -d " <>
          "--name #{name()}-postgres " <>
          "-e POSTGRES_PASSWORD=postgres " <>
          "-p 5432:5432 " <>
          "-v ${PWD}/.volumes/#{name()}-postgres:/var/lib/postgresql/data " <>
          "postgres:12"
      ],
      "postgres.delete": [
        "cmd docker rm -f #{name()}-postgres",
        "cmd rm -rf ${PWD}/.volumes"
      ],

      # Database
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"],

      # Docker
      "docker.build": ["cmd docker build --build-arg MIX_ENV=#{Mix.env()} -t #{docker_tag()} ."],
      "docker.push": ["cmd docker push #{docker_tag()}"],
      "docker.sha256": [
        ~s(cmd docker inspect --format='"{{index .Id}}"' #{docker_tag()})
      ],

      # Helm
      "helm.push": [
        "cmd cd #{helm_dir()} && helm push . #{get_env(:HELM_REPO)} --username=\"#{
          get_env(:HELM_REPO_USERNAME)
        }\" --password=\"#{get_env(:HELM_REPO_PASSWORD)}\""
      ],
      "helm.delete": ["cmd helm delete --namespace #{namespace()} #{name()}"],
      "helm.install": ["cmd #{createHelmInstall()}"],
      "helm.install.local": ["cmd #{createHelmInstall("#{helm_dir()}/values-local.yaml")}"],
      "helm.upgrade": ["cmd #{createHelmUpgrade()}"],
      "helm.upgrade.local": ["cmd #{createHelmUpgrade("#{helm_dir()}/values-local.yaml")}"],
      helm: [
        "docker.build",
        "docker.push",
        "helm.upgrade"
      ],
      "helm.local": [
        "docker.build",
        "docker.push",
        "helm.upgrade.local"
      ]
    ]
  end
end

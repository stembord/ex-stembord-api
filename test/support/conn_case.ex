defmodule Stembord.Api.ConnCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Phoenix.ConnTest
      alias Stembord.Api.Web.Router.Helpers, as: Routes

      @endpoint Stembord.Api.Web.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Stembord.Api.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Stembord.Api.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end

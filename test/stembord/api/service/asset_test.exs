defmodule Stembord.Api.Service.AssetTest do
  use Stembord.Api.ServiceCase

  alias Stembord.Api.Service
  alias Stembord.Api.Model

  @test_png "test/fixtures/test.png"
  defp test_upload(name \\ "test"), do: %Plug.Upload{
    filename: "#{name}.png",
    content_type: "image/png",
    path: @test_png
  }

  describe "asset" do
    test "should create asset" do
      real_hash = Service.Asset.Create.hash_file(@test_png)

      asset_create_command = Service.Asset.Create.new!(%{
        file: test_upload()
      })
      %Model.Asset{ file: %{ file_name: file_name }, hash: hash } = Service.Asset.Create.handle!(asset_create_command)
      assert hash == real_hash
      assert file_name == "test.png"
    end

    test "should return previous asset if hashes are the same" do
      asset_create_first_command = Service.Asset.Create.new!(%{
        file: test_upload("first"),
      })
      %Model.Asset{ id: first_id } = Service.Asset.Create.handle!(asset_create_first_command)

      asset_create_second_command = Service.Asset.Create.new!(%{
        file: test_upload("second"),
      })
      %Model.Asset{ id: second_id } = Service.Asset.Create.handle!(asset_create_second_command)

      assert first_id == second_id
    end
  end
end

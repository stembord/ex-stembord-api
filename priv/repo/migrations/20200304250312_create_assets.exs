defmodule Stembord.Repo.Migrations.CreateAssets do
  use Ecto.Migration

  def change do
    create table(:assets, primary_key: false) do
      add(:id, :uuid, primary_key: true)

      add(:file, :string, null: false)
      add(:hash, :string, null: false)

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:assets, [:hash]))
  end
end

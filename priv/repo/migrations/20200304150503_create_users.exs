defmodule Stembord.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      
      # FIELDS
      add(:id, :uuid, primary_key: true)
      add(:username, :string, null: false)
      add(:password_hash, :string)
      add(:terms_accepted, :boolean, default: false, null: false)
      add(:is_admin, :boolean, default: false, null: false)

      timestamps(type: :utc_datetime)
    end

    create(unique_index(:users, [:username]))
  end
end

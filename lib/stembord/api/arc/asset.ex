defmodule Stembord.Api.Arc.Asset do
  use Arc.Definition
  use Arc.Ecto.Definition

  def filename(_version, {file, asset}) do
    Path.basename(asset.hash, Path.extname(file.file_name))
  end
end
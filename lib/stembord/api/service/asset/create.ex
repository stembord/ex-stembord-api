defmodule Stembord.Api.Service.Asset.Create do
  use Aicacia.Handler
  use Arc.Ecto.Schema 
  import Ecto.Changeset

  alias Stembord.Api.Service.Asset.Create
  alias Stembord.Api.Repo
  alias Stembord.Api.Model

  schema "" do
    field(:file, :map)
    field(:hash, :string)
  end

  def changeset(%{} = params) do
    %Create{}
    |> cast(params, [:file])
    |> generate_file_hash()
    |> validate_required([:file, :hash])
  end

  def handle(%{} = command) do
    Repo.transaction(fn ->
      case Repo.get_by(Model.Asset, hash: command.hash) do
        nil ->
          create_asset!(command)
        asset ->
          asset
      end
    end)
  end

  def create_asset!(%{} = attrs) do
    %Model.Asset{}
    |> cast(attrs, [:hash])
    |> cast_attachments(attrs, [:file])
    |> validate_required([:file, :hash])
    |> unique_constraint(:hash)
    |> Repo.insert!()
  end

  def generate_file_hash(%Ecto.Changeset{} = changeset) do
    file = get_field(changeset, :file)

    if file == nil do
      changeset
      |> add_error(:file, "file attachment required", validation: :file)
    else
      changeset
      |> put_change(:hash, hash_file(file.path))
    end
  end

  def hash_file(path) do
    File.stream!(path, [], 2048)
    |> Enum.reduce(:crypto.hash_init(:sha256), fn line, acc ->
      :crypto.hash_update(acc, line)
    end)
    |> :crypto.hash_final()
    |> Base.encode16()
  end
end

defmodule Stembord.Api.Service.User.Register do
    use Aicacia.Handler
  
    alias Stembord.Api.Service.User.Repo.User, as: UserRepo

    schema "" do
      field(:username, :string)
      field(:password, :string)
      field(:password_confirmation, :string)
      field(:is_admin, :boolean)
    end
  
    def changeset(%{} = params), do: UserRepo.registration_changeset(params)
  
    def handle(%{} = command) do
      Stembord.Api.Repo.transaction(fn ->
        new_user = UserRepo.register!(command)
        new_user
      end)
    end
  end
  
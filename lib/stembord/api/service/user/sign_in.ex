defmodule Stembord.Api.Service.User.SignIn do
    use Aicacia.Handler
  
    alias Stembord.Api.Model.User
    alias Stembord.Api.Repo
    alias Stembord.Api.Service.User.Repo.User, as: UserRepo

    schema "" do
      field(:username, :string)
      field(:password, :string)
    end
  
    def changeset(%{} = params) do
        %User{}
        |> cast(params, [:username, :password])
        |> validate_required([:username, :password])
    end
  
    def handle(%{ username: username, password: password } = _command) do
      Repo.transaction(fn -> 
        {:ok, user} = UserRepo.find_and_confirm_password(username, password)
        user
      end)
    end
  end
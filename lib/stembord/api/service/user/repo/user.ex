defmodule Stembord.Api.Service.User.Repo.User do
  import Ecto.Query, warn: false
  import Ecto.Changeset
  
  
  alias Stembord.Api.Repo
  alias Stembord.Api.Model.User

  @valid_username ~r/\A[a-z]+[a-z0-9]*\z/

  def get(id), do: Repo.get(User, id)
  def get!(id), do: Repo.get!(User, id)
  def get_by(search), do: Repo.get_by(User, search)
  def get_by!(search), do: Repo.get_by!(User, search)

  def all(), do: Repo.all(User)

  def create!(%{} = attrs) do
    %User{}
    |> cast(attrs, [:username, :password_hash])
    |> validate_required([:username])
    |> unique_constraint(:name)
    |> Repo.insert!()
  end

  def update!(%User{} = user, attrs \\ %{}) do
    user
    |> cast(attrs, [:name, :schedule])
    |> Repo.update!()
  end

  def delete!(%User{} = user) do
    Repo.delete!(user)
  end

  def find_and_confirm_password(username, password) do
    Repo.get_by!(User, username: username) |> Argon2.check_pass(password)
  end

  def register!(attrs \\ %{}) do
    %User{}
    |> cast(attrs, [:username, :password_hash, :is_admin])
    |> validate_required([:username, :password_hash])
    |> unique_constraint(:username)
    |> Repo.insert!()
  end

  def registration_changeset(attrs) do
    %User{}
    |> cast(attrs, [:username, :password, :password_confirmation, :is_admin])
    |> validate_required([:username, :password, :password_confirmation])
    |> validate_format(:username, @valid_username)
    |> validate_length(:username, min: 3)
    |> validate_length(:password, min: 8)
    |> validate_confirmation(:password)
    |> unique_constraint(:username)
    |> put_password_hash()
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pw}}
        ->
          put_change(changeset, :password_hash, Argon2.hash_pwd_salt(pw))
      _ ->
          changeset
    end
  end
end

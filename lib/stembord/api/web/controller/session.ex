defmodule Stembord.Api.Web.Controller.Session do

    use Stembord.Api.Web, :controller

    action_fallback Stembord.Api.Web.Controller.Fallback
    
    plug :put_view, Stembord.Api.Web.View.SessionView

    require Logger

    alias Stembord.Api.Service.User.SignIn
    alias Stembord.Api.Service.User.Token

    def sign_in(conn, %{"session" => session_params} = _params) do
        cmd = SignIn.new!(session_params)
        with {:ok, user} <- SignIn.handle(cmd),
        {:ok, jwt, _claims} <- Token.generate_and_sign(%{"id" => user.id}) do
            conn
            |> render("sign_in.json", user: user, jwt: jwt)
        end
    end
    def sign_in(conn, _params), do:
        conn
        |> put_status(422)
        |> render("error.json", message: "invalid request parameters")


    def current(conn, _params) do
        user = conn.assigns[:user]
        render(conn, "user.json", user: user)
    end

    def sign_out(conn, _params) do
        send_resp(conn, :ok, "")
    end

end
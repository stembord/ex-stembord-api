defmodule Stembord.Api.Web.Controller.Fallback do
  use Stembord.Api.Web, :controller
  
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(Stembord.Api.Web.View.Changeset)
    |> render("error.json", changeset: changeset)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(Stembord.Api.Web.View.Error)
    |> render(:"404")
  end
  
end

defmodule Stembord.Api.Web.Controller.HealthCheck do
  use Stembord.Api.Web, :controller
  import OpenApiSpex.Operation, only: [response: 3]
  alias OpenApiSpex.Operation
  alias Stembord.Api.Web.Schema

  plug OpenApiSpex.Plug.CastAndValidate

  action_fallback Stembord.Api.Web.Controller.Fallback

  def open_api_operation(action) do
    apply(__MODULE__, :"#{action}_operation", [])
  end

  def health_check_operation() do
    %Operation{
      tags: ["health_check"],
      summary: "health check",
      description: "health check",
      operationId: "HealthCheck.health",
      responses: %{
        200 => response("Health check ok response", "application/json", Schema.HealthCheck.OkResponse)
      }
    }
  end
  
  def health_check(conn, _params) do
    conn
    |> put_view(Stembord.Api.Web.View.HealthCheck)
    |> render("ok.json")
  end
end

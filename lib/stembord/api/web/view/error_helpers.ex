defmodule Stembord.Api.Web.View.ErrorHelpers do
  def translate_error({msg, opts}) do
    if count = opts[:count] do
      Gettext.dngettext(Stembord.Api.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(Stembord.Api.Gettext, "errors", msg, opts)
    end
  end
end

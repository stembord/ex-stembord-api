defmodule Stembord.Api.Web.View.ErrorsView do
    def render("401.json", %{message: msg}) do
        %{
            status: :error,
            data: %{ message: msg },
            message: msg
        }
    end
end
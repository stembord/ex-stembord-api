defmodule Stembord.Api.Web.View.HealthCheck do
  use Stembord.Api.Web, :view

  def render("ok.json", %{}) do
    %{ok: true}
  end
end

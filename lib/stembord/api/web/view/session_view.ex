defmodule Stembord.Api.Web.View.SessionView do
    use Stembord.Api.Web, :view

    def render("sign_in.json", %{user: user, jwt: jwt}) do
        %{
            status: :ok,
            data: %{ token: jwt, username: user.username },
            message: "You are successfully logged in! Add this token to authorization header to make authorized requests."
        }
    end

    def render("user.json", %{user: user}) do
        %{
            data: %{ username: user.username }
        }
    end

    def render("error.json", %{message: msg}) do
        %{
            status: :error,
            data: %{ message: msg },
            message: msg
        }
    end

end
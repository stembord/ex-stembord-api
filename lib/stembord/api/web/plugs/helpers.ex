defmodule Stembord.Api.Web.Plugs.Helpers do
    use Stembord.Api.Web, :plugs

    def unauthorized(conn) do
        conn
        |> put_status(401)
        |> render(Stembord.Api.Web.View.ErrorsView, "401.json", %{message: "unauthorized"})
        |> halt()
    end

end

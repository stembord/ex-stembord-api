defmodule Stembord.Api.Web.Plugs.Authenticate do
    use Stembord.Api.Web, :plugs
    import Stembord.Api.Web.Plugs.Helpers, only: [unauthorized: 1]
  
    alias Stembord.Api.Service.User.Repo.User, as: UserRepo
    alias Stembord.Api.Model.User
    alias Stembord.Api.Service.User.Token

    require Logger
  
    def init(opts), do: opts
  
    def call(conn, _opts) do
      token = get_req_header(conn, "authorization") |> List.first()
      authorize_connection(conn, token)
    end
  
    defp authorize_connection(conn, nil), do: unauthorized(conn)
  
    defp authorize_connection(conn, token) do
        case Token.verify_and_validate(token) do
            {:ok, %{"id" => user_id}} ->
                assign_auth_data(conn, token, UserRepo.get!(user_id))

            _otherwise ->
                unauthorized(conn)
        end
    end
  
    defp assign_auth_data(conn, _token, nil), do: unauthorized(conn)
  
    defp assign_auth_data(conn, token, %User{} = user) do
      conn
      |> assign(:token, token)
      |> assign(:user, user)
    end
  end
  
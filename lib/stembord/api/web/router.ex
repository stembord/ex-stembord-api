defmodule Stembord.Api.Web.Router do
  use Stembord.Api.Web, :router

  pipeline :browser do
    plug(:accepts, ["html"])
  end

  pipeline :api do
    plug(:accepts, ["json"])
    plug(OpenApiSpex.Plug.PutApiSpec, module: Stembord.Api.Web.Spec)
  end

  pipeline :authenticate do
    plug(Stembord.Api.Web.Plugs.Authenticate)
  end

  if Mix.env == :dev do
    scope "/docs" do
      pipe_through(:browser)
      get("/", OpenApiSpex.Plug.SwaggerUI, path: "/openapi")
    end
  end

  scope "/", Stembord.Api.Web do
    pipe_through([:api])

    get("/health", Controller.HealthCheck, :health_check)
    get("/openapi", OpenApiSpex.Plug.RenderSpec, :show)

    post("/sessions/sign_in", Controller.Session, :sign_in)

  end

  scope "/", Stembord.Api.Web do
    pipe_through([:api, :authenticate])

    get("/sessions/current", Controller.Session, :current)
    delete("/sessions/sign_out", Controller.Session, :sign_out)

  end
end

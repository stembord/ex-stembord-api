defmodule Stembord.Api.Web.Schema.HealthCheck do
  require OpenApiSpex
  alias OpenApiSpex.Schema

  defmodule OkResponse do
    OpenApiSpex.schema(%{
      title: "Health Check",
      description: "health check response",
      type: :object,
      properties: %{
        ok: %Schema{type: :boolean, description: "Okay status"}
      },
      required: [:ok],
      example: %{
        "ok" => true
      }
    })
  end
end
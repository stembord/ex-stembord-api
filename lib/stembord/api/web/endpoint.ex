defmodule Stembord.Api.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :stembord_api

  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  plug CORSPlug

  plug Plug.Static, from: {:stembord_api, "priv/static/assets"}, at: "/static/assets"
  plug Stembord.Api.Web.Router
end

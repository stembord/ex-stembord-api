defmodule Stembord.Api.Web do
  def controller do
    quote do
      use Phoenix.Controller, namespace: Stembord.Api.Web

      import Plug.Conn
      import Stembord.Api.Gettext
      alias Stembord.Api.Web.Router.Helpers, as: Routes
    end
  end

  def plugs do
    quote do
      use Phoenix.Controller, namespace: Stembord.Api.Web
      import Plug.Conn
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/stembord/api/web/templates",
        namespace: Stembord.Api.Web

      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      import Stembord.Api.Web.View.ErrorHelpers
      import Stembord.Api.Gettext
      alias Stembord.Api.Web.Router.Helpers, as: Routes
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import Stembord.Api.Gettext
    end
  end

  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end

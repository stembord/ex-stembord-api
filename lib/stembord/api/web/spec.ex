defmodule Stembord.Api.Web.Spec do
  alias OpenApiSpex.{Info, OpenApi, Paths}
  @behaviour OpenApi

  @impl OpenApi
  def spec do
    %OpenApi{
      info: %Info{
        title: "Stembord Api",
        version: "0.1"
      },
      servers: [OpenApiSpex.Server.from_endpoint(Stembord.Api.Web.Endpoint)],
      paths: Paths.from_router(Stembord.Api.Web.Router)
    }
    |> OpenApiSpex.resolve_schema_modules()
  end
end
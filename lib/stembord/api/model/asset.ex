defmodule Stembord.Api.Model.Asset do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "assets" do
    field(:file, Stembord.Api.Arc.Asset.Type, null: false)
    field(:hash, :string, null: false)

    timestamps(type: :utc_datetime)
  end
end

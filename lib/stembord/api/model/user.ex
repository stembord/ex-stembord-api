defmodule Stembord.Api.Model.User do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    field(:username, :string)
    field(:password_hash, :string)
    field(:terms_accepted, :boolean, default: false)
    field(:is_admin, :boolean, default: false)

    # virtual fields
    field(:password, :string, virtual: true)
    field(:password_confirmation, :string, virtual: true)

    timestamps(type: :utc_datetime)
  end

end

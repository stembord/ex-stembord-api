defmodule Stembord.Api.Application do
  use Application
  import Supervisor.Spec, warn: false

  def start(_type, _args) do
    children = [
      supervisor(Stembord.Api.Repo, []),
      supervisor(Stembord.Api.Web.Endpoint, [])
    ]

    opts = [strategy: :one_for_one, name: Stembord.Api.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    Stembord.Api.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
